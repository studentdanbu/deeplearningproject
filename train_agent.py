import numpy as np
import tensorflow as tf
import numpy as np
import pickle

# custom modules
from utils     import Options, rgb2gray
from simulator import Simulator
from transitionTable import TransitionTable
from agent import Agent

def append_to_hist(state, obs):
    """
    Add observation to the state.
    """
    for i in range(state.shape[0]-1):
        state[i, :] = state[i+1, :]
    state[-1, :] = obs

# intizialize training
opt = Options()
sim = Simulator(opt.map_ind, opt.cub_siz, opt.pob_siz, opt.act_num)

agent = Agent()
sess = tf.Session()
sess.run(tf.global_variables_initializer())

saver = tf.train.Saver()
#saver.restore(sess, "./data/policies.ckpt")

stats = []

num_batches = 10#2000
num_episodes = 200#50
solved_episodes = 0

for batch_i in range(num_batches):
    episode_batch = []
    average_steps = 0

    for episode_i in range(num_episodes):
        episode = []

        state = sim.newGame(opt.tgt_y, opt.tgt_x)

        state_with_history = np.zeros((opt.hist_len, opt.state_siz))
        agent.clear_hist()

        steps = 0
        while not state.terminal and steps < opt.early_stop:
            steps += 1
            # simulation step
            agent.append_to_hist(rgb2gray(state.pob).reshape(opt.state_siz))
            append_to_hist(state_with_history, rgb2gray(state.pob).reshape(opt.state_siz))

            epsilon = 0.1#schedule(episode_i) # TODO: make nicer if this stays in
            action = agent.make_action(sess)
            next_state = sim.step(action)

            episode.append((state_with_history, action, next_state.reward))
            state = next_state

            if state.terminal:
                solved_episodes += 1
        average_steps += steps
        episode_batch.append(episode)

    # perfom training
    average_steps /= num_episodes
    average_reward = agent.reinforce(sess, episode_batch)
    print("\rBatch {}/{} ({})".format(# | Solved: {}".format(
        batch_i + 1,
        num_batches,
        average_reward))
        #solved_episodes), end=" ")

    # save stats
    stats.append((average_steps, average_reward))

# save stats
with open("./stats", 'wb') as f:
    pickle.dump(stats, f)

# svae network
saver.save(sess, "./data/policies.ckpt")
