import tensorflow as tf
import numpy as np
from utils import Options
import math # DEBUG: for assertions only

opt = Options()

class Agent():

    def __init__(self):
        # Store the history of the observed states
        self.history = np.zeros((opt.hist_len, opt.state_siz))

        # Build the tensorflow graph
        self.states = tf.placeholder(tf.float32, shape=(None, opt.hist_len*opt.state_siz))
        self.actions = tf.placeholder(tf.int32, shape=(None))
        self.targets = tf.placeholder(tf.float32, shape=(None))

        self.predictions = self.forward_pass(self.states)
        self.best_actions_next = tf.argmax(self.predictions, axis=1)

        # Get the predictions for the chosen actions only
        batch_size = tf.shape(self.states)[0]
        gather_indices = tf.range(batch_size) * tf.shape(self.predictions)[1] + self.actions
        self.action_predictions = tf.gather(tf.reshape(self.predictions, [-1]), gather_indices)

        # Define the policy gradient objective (negative as we want to maximize).
        self.objective = tf.reduce_mean(-tf.log(tf.maximum(self.action_predictions,1e-10)) * self.targets)

        # Setup an optimizer in tensorflow to minimize the loss
        self.train_step = tf.train.AdamOptimizer(opt.learning_rate).minimize(self.objective)

    def forward_pass(self, states):
        """
        Defines structure of network, returns one output for each possible action.
        """
        input_layer = tf.reshape(
            states,
            [-1, opt.pob_siz * opt.cub_siz, opt.pob_siz * opt.cub_siz, opt.hist_len])

        out_conv1 = tf.layers.conv2d(
            inputs=input_layer,
            filters=opt.num_filters,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_conv2 = tf.layers.conv2d(
            inputs=out_conv1,
            filters=opt.num_filters,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        flat_shape = int(out_conv2.shape[1]*out_conv2.shape[2]*out_conv2.shape[3])
        out_conv2_flat = tf.reshape(out_conv2, [-1, flat_shape])

        out_fcon1 = tf.layers.dense(
            inputs=out_conv2_flat,
            units=opt.num_units_linear_layer,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_drop = tf.layers.dropout(out_fcon1, rate=0.5, training=True)

        out_fcon2 = tf.layers.dense(
            inputs=out_drop,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            units=opt.act_num,
            activation = None)

        predictions = tf.nn.softmax(out_fcon2)
        return predictions

    def append_to_hist(self, obs):
        """
        Add observation to the history.
        """
        for i in range(self.history.shape[0]-1):
            self.history[i, :] = self.history[i+1, :]
        self.history[-1, :] = obs

    def clear_hist(self):
        """
        Clear history if new episode starts.
        """
        self.history[:] = 0

    def make_greedy_action(self, sess):
        dict = {self.states: [self.history.reshape(-1)]}
        best_action = sess.run(self.best_actions_next, feed_dict=dict)[0]
        return best_action

    def make_action(self, sess):
        dict = {self.states: [self.history.reshape(-1)]}
        _predictions = sess.run(self.predictions, feed_dict=dict)[0]

        # #DEBUG
        # total=0.
        # for x in _predictions:
        #     total+=x
        # if total > 1:
        #     print(total)
        #     print(_predictions)
        for p in _predictions:
            assert not math.isnan(p)
        # #END DEBUG

        action = np.random.choice(np.arange(opt.act_num), p=_predictions)
        return action

    def train(self, sess, state_batch, action_batch, target_batch):
        """
        Updates weights of network.
        """
        dict = {
            self.states : state_batch,
            self.actions : action_batch,
            self.targets : target_batch}
        sess.run(self.train_step, feed_dict = dict)
        #return sess.run(self.objective, feed_dict = dict)

    def reinforce(self, sess, episode_batch):
        """
        TODO: batch reinforce algorithm
        """
        state_batch = []
        action_batch = []
        target_batch = []

        for episode in episode_batch:
            for t in range(len(episode)):
                v_t = sum(opt.discount_factor**i * r for i, (s, a, r) in enumerate(episode[t:]))
                state, action, reward = episode[t]

                state_batch.append(state.reshape(-1))
                action_batch.append(action)
                target_batch.append(v_t)

        target_batch = (target_batch - np.mean(target_batch)) / (np.std(target_batch) + 1e-10)
        self.train(sess, state_batch, action_batch, target_batch)

        average_reward = 0
        for episode in episode_batch:
            average_reward += sum(r for i, (s, a, r) in enumerate(episode))
        average_reward /= len(episode_batch)
        return average_reward


    def print_predictions(self, sess):
        # DEBUG
        dict = {self.states: [self.history.reshape(-1)]}
        _predictions = sess.run(self.predictions, feed_dict=dict)[0]
        print(_predictions)
